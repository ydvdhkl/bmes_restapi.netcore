﻿using BMES_RestAPI.Messages.Request.Order;
using BMES_RestAPI.Messages.Response.Order;

namespace BMES_RestAPI.Services
{
    public interface IOrderService
    {
        GetOrderResponse GetOrder(GetOrderRequest getOrderRequest);
        FetchOrdersResponse GetOrders(FetchOrdersRequest fetchOrdersRequest);
    }
}
