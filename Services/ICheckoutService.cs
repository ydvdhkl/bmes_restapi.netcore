﻿using BMES_RestAPI.Messages.Request.Checkout;
using BMES_RestAPI.Messages.Response.Checkout;

namespace BMES_RestAPI.Services
{
    public interface ICheckoutService
    {
        CheckoutResponse ProcessCheckout(CheckoutRequest checkoutRequest);
    }
}
