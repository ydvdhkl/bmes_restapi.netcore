﻿using BMES_RestAPI.Messages.Request.User;
using BMES_RestAPI.Messages.Response.User;
using System.Threading;
using System.Threading.Tasks;

namespace BMES_RestAPI.Services
{
    public interface IAuthService
    {
        Task<RegisterResponse> RegisterAsync(RegisterRequest request, CancellationToken cancellationToken = default);
        Task<LogInResponse> LogInAsync(LogInRequest request, CancellationToken cancellationToken = default);
    }
}
