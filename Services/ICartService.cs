﻿using BMES_RestAPI.Messages.Request.Cart;
using BMES_RestAPI.Messages.Response.Cart;
using BMES_RestAPI.Models.Cart;
using System.Collections.Generic;

namespace BMES_RestAPI.Services
{
    public interface ICartService
    {
        AddItemToCartResponse AddItemToCart(AddItemToCartRequest addItemToCartRequest);
        RemoveItemFromCartResponse RemoveItemFromCart(RemoveItemFromCartRequest removeItemFromCartRequest);
        string UniqueCartId();
        Cart GetCart();
        FetchCartResponse FetchCart();
        IEnumerable<CartItem> GetCartItems();
        int CartItemsCount();
        decimal GetCartTotal();
    }
}
