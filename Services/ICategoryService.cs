﻿using BMES_RestAPI.Messages.Request.Category;
using BMES_RestAPI.Messages.Response.Category;

namespace BMES_RestAPI.Services
{
    public interface ICategoryService
    {
        FetchCategoriesResponse GetCategories(FetchCategoryRequest fetchCategoryRequest);
        GetCategoryResponse GetCategory(GetCategoryRequest getCategoryRequest);
        UpdateCategoryResponse EditCategory(UpdateCategoryRequest updateCategoryRequest);
        CreateCategoryResponse SaveCategory(CreateCategoryRequest createCategoryRequest);
        DeleteCategoryResponse DeleteCategory(DeleteCategoryRequest deleteCategoryRequest);
    }
}
