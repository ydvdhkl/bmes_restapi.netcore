﻿using BMES_RestAPI.Messages.Request.Product;
using BMES_RestAPI.Messages.Response.Product;

namespace BMES_RestAPI.Services
{
    public interface ICatalogueService
    {
        FetchProductsResponse FetchProducts(FetchProductsRequest fetchProductsRequest);
    }
}
