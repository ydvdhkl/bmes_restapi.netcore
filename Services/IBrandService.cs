﻿using BMES_RestAPI.Messages.Request.Brand;
using BMES_RestAPI.Messages.Response.Brand;

namespace BMES_RestAPI.Services
{
    public interface IBrandService
    {
        FetchBrandsResponse GetBrands(FetchBrandsRequest fetchBrandsRequest);
        GetBrandResponse GetBrand(GetBrandRequest getBrandRequest);
        UpdateBrandResponse EditBrand(UpdateBrandRequest updateBrandRequest);
        CreateBrandResponse SaveBrand(CreateBrandRequest brandRequest);
        DeleteBrandResponse DeleteBrand(DeleteBrandRequest deleteBrandRequest);
    }
}
