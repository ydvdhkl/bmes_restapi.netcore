﻿namespace BMES_RestAPI.Models.Shared
{
    public enum UserRole
    {
        Unknown = 0,
        Administrator = 1,
        RegisteredUser = 2
    }
}
