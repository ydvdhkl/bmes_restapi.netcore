﻿using Microsoft.AspNetCore.Identity;

namespace BMES_RestAPI.Models.Shared
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
    }
}
