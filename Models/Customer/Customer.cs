﻿using BMES_RestAPI.Models.Shared;
using System.Collections.Generic;

namespace BMES_RestAPI.Models.Customer
{
    using Address;
    using Order;
    public class Customer : BaseObject
    {
        public long PersonId { get; set; }
        public Person Person { get; set; }
        public IEnumerable<Address> Addresses { get; set; }
        public IEnumerable<Order> Orders { get; set; }
    }
}
