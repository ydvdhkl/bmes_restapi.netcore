﻿using BMES_RestAPI.Models.Shared;

namespace BMES_RestAPI.Models.Order
{
    using Product;
    public class OrderItem : BaseObject
    {
        public long OrderId { get; set; }
        public Order Order { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
