﻿using BMES_RestAPI.Models.Shared;
using System.Collections.Generic;

namespace BMES_RestAPI.Models.Cart
{
    public class Cart : BaseObject
    {
        public Cart()
        {
            CartItems = new List<CartItem>();
        }
        public string UniqueCartId { get; set; }
        public CartStatus CartStatus { get; set; }
        public IEnumerable<CartItem> CartItems { get; set; }
    }
}
