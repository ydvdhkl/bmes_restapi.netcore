﻿namespace BMES_RestAPI.Models.Cart
{
    public enum CartStatus
    {
        Open = 0,
        CheckedOut = 1
    }
}
