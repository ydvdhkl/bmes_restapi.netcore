﻿using BMES_RestAPI.Models.Product;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAllProducts();
        Product FindProductById(long id);
        void SaveProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);
    }
}
