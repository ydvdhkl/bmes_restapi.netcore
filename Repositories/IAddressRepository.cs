﻿using BMES_RestAPI.Models.Address;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface IAddressRepository
    {
        IEnumerable<Address> GetAllAddresses();
        Address FindAddressById(long id);
        void SaveAddress(Address address);
        void UpdateAddress(Address address);
        void DeleteAddress(Address address);
    }
}
