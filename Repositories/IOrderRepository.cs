﻿using BMES_RestAPI.Models.Order;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface IOrderRepository
    {
        Order FindOrderById(long id);
        IEnumerable<Order> GetAllOrders();
        void SaveOrder(Order order);
        void UpdateOrder(Order order);
        void DeleteOrder(Order order);
    }
}
