﻿using BMES_RestAPI.Models.Cart;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface ICartRepository
    {
        IEnumerable<Cart> GetAllCarts();
        Cart FindCartById(long id);
        void SaveCart(Cart cart);
        void UpdateCart(Cart cart);
        void DeleteCart(Cart cart);
    }
}
