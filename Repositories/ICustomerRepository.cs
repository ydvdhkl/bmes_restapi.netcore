﻿using BMES_RestAPI.Models.Customer;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAllCustomers();
        Customer FindCustomerById(long id);
        void SaveCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
        void DeleteCustomer(Customer customer);
    }
}
