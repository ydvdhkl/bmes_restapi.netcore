﻿using BMES_RestAPI.Models.Shared;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface IPersonRepository
    {
        IEnumerable<Person> GetAllPeople();
        Person FindPersonById(long id);
        void SavePerson(Person person);
        void UpdatePerson(Person person);
        void DeletePerson(Person person);
    }
}
