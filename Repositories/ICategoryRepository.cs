﻿using BMES_RestAPI.Models.Product;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetAllCategories();
        Category FindCategoryById(long id);
        void SaveCategory(Category category);
        void UpdateCategory(Category category);
        void DeleteCategory(Category category);
    }
}
