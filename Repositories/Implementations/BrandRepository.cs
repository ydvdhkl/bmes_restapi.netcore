﻿
using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Product;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class BrandRepository : IBrandRepository
    {
        private BMES_DBContext _dBContext;

        public BrandRepository(BMES_DBContext dBContext)
        {
            _dBContext = dBContext;
        }
        public IEnumerable<Brand> GetAllBrands()
        {
            var brands = _dBContext.Brands;
            return brands;
        }
        public Brand FindBrandById(long id)
        {
            var brand = _dBContext.Brands.Find(id);
            return brand;
        }

        public void SaveBrand(Brand brand)
        {
            _dBContext.Brands.Add(brand);
            _dBContext.SaveChanges();
        }

        public void UpdateBrand(Brand brand)
        {
            _dBContext.Brands.Update(brand);
            _dBContext.SaveChanges();
        }
        public void DeleteBrand(Brand brand)
        {
            _dBContext.Brands.Remove(brand);
            _dBContext.SaveChanges();
        }

    }
}
