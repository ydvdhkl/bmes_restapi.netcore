﻿using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Customer;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class CustomerRepository : ICustomerRepository
    {
        private BMES_DBContext _context;
        public CustomerRepository(BMES_DBContext context)
        {
            _context = context;
        }
        public IEnumerable<Customer> GetAllCustomers()
        {
            var customers = _context.Customers;
            return customers;
        }
        public Customer FindCustomerById(long id)
        {
            var customer = _context.Customers.Find(id);
            return customer;
        }

        public void SaveCustomer(Customer customer)
        {
            _context.Customers.Add(customer);
            _context.SaveChanges();
        }

        public void UpdateCustomer(Customer customer)
        {
            _context.Customers.Update(customer);
            _context.SaveChanges();
        }
        public void DeleteCustomer(Customer customer)
        {
            _context.Customers.Remove(customer);
            _context.SaveChanges();
        }
    }
}
