﻿using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class OrderItemRepository : IOrderItemRepository
    {
        private BMES_DBContext _context;
        public OrderItemRepository(BMES_DBContext context)
        {
            _context = context;  
        }
        public IEnumerable<OrderItem> GetAllOrderItems()
        {
            var orderItems = _context.OrderItems;
            return orderItems;
        }
        public OrderItem FindOrderItemById(long id)
        {
            var orderItem = _context.OrderItems.Find(id);
            return orderItem;
        }
        public IEnumerable<OrderItem> FindOrderItemByOrderId(long orderId)
        {
            var orderItems = _context.OrderItems.Where(o => o.OrderId == orderId);
            return orderItems;
        }
        public void SaveOrderItem(OrderItem orderItem)
        {
            _context.OrderItems.Add(orderItem);
        }

        public void UpdateOrderItem(OrderItem orderItem)
        {
            _context.OrderItems.Update(orderItem);
        }
        public void DeleteOrderItem(OrderItem orderItem)
        {
            _context.OrderItems.Remove(orderItem);
        }
    }
}
