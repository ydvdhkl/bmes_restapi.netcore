﻿using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Cart;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class CartRepository : ICartRepository
    {
        private BMES_DBContext _context;

        public CartRepository(BMES_DBContext context)
        {
            _context = context;
        }

        public IEnumerable<Cart> GetAllCarts()
        {
            var carts = _context.Carts;
            return carts;
        }

        public Cart FindCartById(long id)
        {
            var cart = _context.Carts.Find(id);
            return cart;
        }

        public void SaveCart(Cart cart)
        {
            _context.Carts.Add(cart);
            _context.SaveChanges();
        }

        public void UpdateCart(Cart cart)
        {
            _context.Carts.Update(cart);
            _context.SaveChanges();
        }
        public void DeleteCart(Cart cart)
        {
            _context.Carts.Remove(cart);
            _context.SaveChanges();
        }
    }
}
