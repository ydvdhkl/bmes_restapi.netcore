﻿using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Address;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class AddressRepository : IAddressRepository
    {
        private BMES_DBContext _context;
        public AddressRepository(BMES_DBContext context)
        {
            _context = context;
        }
        public IEnumerable<Address> GetAllAddresses()
        {
            var addresses = _context.Addresses;
            return addresses;
        }
       
        public Address FindAddressById(long id)
        {
            var address = _context.Addresses.Find(id);
            return address;
        }

        public void SaveAddress(Address address)
        {
            _context.Addresses.Add(address);
            _context.SaveChanges();
        }

        public void UpdateAddress(Address address)
        {
            _context.Addresses.Update(address);
            _context.SaveChanges();
        }
        public void DeleteAddress(Address address)
        {
            _context.Addresses.Remove(address);
            _context.SaveChanges();
        }
    }
}
