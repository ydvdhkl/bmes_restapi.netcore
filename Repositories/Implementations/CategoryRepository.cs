﻿
using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Product;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class CategoryRepository : ICategoryRepository
    {
        private BMES_DBContext _dBContext;

        public CategoryRepository(BMES_DBContext dBContext)
        {
            _dBContext = dBContext;
        }
        public IEnumerable<Category> GetAllCategories()
        {
            var categories = _dBContext.Categories;
            return categories;
        }
        public Category FindCategoryById(long id)
        {
            var category = _dBContext.Categories.Find(id);
            return category;
        }
        public void SaveCategory(Category category)
        {
            _dBContext.Categories.Add(category);
            _dBContext.SaveChanges();
        }
        public void UpdateCategory(Category category)
        {
            _dBContext.Categories.Update(category);
            _dBContext.SaveChanges();
        }
        public void DeleteCategory(Category category)
        {
            _dBContext.Categories.Remove(category);
            _dBContext.SaveChanges();
        }
    }
}
