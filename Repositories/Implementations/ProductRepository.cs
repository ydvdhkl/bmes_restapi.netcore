﻿using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Product;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class ProductRepository : IProductRepository
    {
        private BMES_DBContext _dBContext;

        public ProductRepository(BMES_DBContext dBContext)
        {
            _dBContext = dBContext;
        }
        public IEnumerable<Product> GetAllProducts()
        {
            var products = _dBContext.Products.Include(navigationPropertyPath: p => p.Category).Include(navigationPropertyPath: p => p.Brand);
            return products;
        }
        public Product FindProductById(long id)
        {
            var product = _dBContext.Products.Find(id);
            return product;
        }
        public void SaveProduct(Product product)
        {
            _dBContext.Products.Add(product);
            _dBContext.SaveChanges();
        }
        public void UpdateProduct(Product product)
        {
            _dBContext.Products.Update(product);
            _dBContext.SaveChanges();
        }
        public void DeleteProduct(Product product)
        {
            _dBContext.Products.Remove(product);
            _dBContext.SaveChanges();
        }
    }
}
