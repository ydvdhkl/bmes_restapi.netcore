﻿using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Shared;
using System;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class PersonRepository : IPersonRepository
    {
        private BMES_DBContext _context;
        public PersonRepository(BMES_DBContext context)
        {
            _context = context;
        }
        public IEnumerable<Person> GetAllPeople()
        {
            var people = _context.Persons;
            return people;
        }
        public Person FindPersonById(long id)
        {
            var person = _context.Persons.Find(id);
            return person;
        }
        public void SavePerson(Person person)
        {
            _context.Persons.Add(person);
            _context.SaveChanges();
        }

        public void UpdatePerson(Person person)
        {
            _context.Persons.Update(person);
            _context.SaveChanges();
        }
        public void DeletePerson(Person person)
        {
            _context.Persons.Remove(person);
            _context.SaveChanges();
        }
    }
}
