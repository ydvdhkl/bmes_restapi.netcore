﻿using BMES_RestAPI.Database;
using BMES_RestAPI.Models.Cart;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BMES_RestAPI.Repositories.Implementations
{
    public class CartItemRepository : ICartItemRepository
    {
        private BMES_DBContext _context;

        public CartItemRepository(BMES_DBContext context)
        {
            _context = context;
        }
        public CartItem FindCartItemById(long id)
        {
            var cartItem = _context.CarItems.Find(id);
            return cartItem;
        }
        public IEnumerable<CartItem> FindCartItemsByCartId(long cartId)
        {
            var cartItems = _context.CarItems.Where(cartitem => cartitem.CartId == cartId).Include(c => c.Product);
            return cartItems;
        }

        public void SaveCartItem(CartItem cartItem)
        {
            _context.CarItems.Add(cartItem);
            _context.SaveChanges();
        }

        public void UpdateCartItem(CartItem cartItem)
        {
            _context.CarItems.Update(cartItem);
            _context.SaveChanges();
        }
        public void DeleteCartItem(CartItem cartItem)
        {
            _context.CarItems.Remove(cartItem);
            _context.SaveChanges();
        }
    }
}
