﻿using BMES_RestAPI.Models.Product;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface IBrandRepository
    {
        IEnumerable<Brand> GetAllBrands();
        Brand FindBrandById(long id);
        void SaveBrand(Brand brand);
        void UpdateBrand(Brand brand);
        void DeleteBrand(Brand brand);
    }
}
