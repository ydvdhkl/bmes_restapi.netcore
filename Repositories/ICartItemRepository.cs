﻿using BMES_RestAPI.Models.Cart;
using System.Collections.Generic;

namespace BMES_RestAPI.Repositories
{
    public interface ICartItemRepository
    {
        IEnumerable<CartItem> FindCartItemsByCartId(long cartId);
        CartItem FindCartItemById(long id);
        void SaveCartItem(CartItem cartItem);
        void UpdateCartItem(CartItem cartItem);
        void DeleteCartItem(CartItem cartItem);
    }
}
