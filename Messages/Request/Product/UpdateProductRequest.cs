﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Product
{
    public class UpdateProductRequest
    {
        public long Id { get; set; }
        public ProductDTO Product { get; set; }
    }
}
