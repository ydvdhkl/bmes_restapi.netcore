﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Product
{
    public class CreateProductRequest
    {
        public ProductDTO Product { get; set; }
    }
}
