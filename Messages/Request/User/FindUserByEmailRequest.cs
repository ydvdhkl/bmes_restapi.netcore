﻿namespace BMES_RestAPI.Messages.Request.User
{
    public class FindUserByEmailRequest
    {
        public string Email { get; set; }
    }
}
