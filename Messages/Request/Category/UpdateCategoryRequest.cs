﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Category
{
    public class UpdateCategoryRequest
    {
        public long Id { get; set; }
        public CategoryDTO Category { get; set; }
    }
}
