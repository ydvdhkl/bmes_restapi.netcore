﻿namespace BMES_RestAPI.Messages.Request.Category
{
    public class DeleteCategoryRequest
    {
        public long Id { get; set; }
    }
}
