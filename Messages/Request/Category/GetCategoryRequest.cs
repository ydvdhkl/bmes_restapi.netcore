﻿namespace BMES_RestAPI.Messages.Request.Category
{
    public class GetCategoryRequest
    {
        public int Id { get; set; }
    }
}
