﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Category
{
    public class CreateCategoryRequest
    {
        public CategoryDTO Category { get; set; }
    }
}
