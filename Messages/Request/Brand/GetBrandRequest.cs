﻿namespace BMES_RestAPI.Messages.Request.Brand
{
    public class GetBrandRequest
    {
        public long Id { get; set; }
    }
}
