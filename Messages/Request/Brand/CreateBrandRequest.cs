﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Brand
{
    public class CreateBrandRequest
    {
        public BrandDTO Brand { get; set; }
    }
}
