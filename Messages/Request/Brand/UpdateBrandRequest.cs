﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Brand
{
    public class UpdateBrandRequest
    {
        public long Id { get; set; }
        public BrandDTO  Brand { get; set; }
    }
}
