﻿namespace BMES_RestAPI.Messages.Request.Brand
{
    public class DeleteBrandRequest
    {
        public long Id { get; set; }
    }
}
