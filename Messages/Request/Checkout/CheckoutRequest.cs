﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Checkout
{
    public class CheckoutRequest
    {
        public CustomerDTO Customer { get; set; }
        public AddressDTO Address { get; set; }
        public CartDTO Cart { get; set; }
    }
}
