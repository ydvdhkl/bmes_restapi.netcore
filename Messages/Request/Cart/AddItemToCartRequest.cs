﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Request.Cart
{
    public class AddItemToCartRequest
    {
        public long CartId { get; set; }
        public CartItemDTO CartItem { get; set; }
        public long ProductId { get; set; }
    }
}
