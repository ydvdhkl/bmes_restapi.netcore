﻿namespace BMES_RestAPI.Messages.Request.Order
{
    public class GetOrderRequest
    {
        public long Id { get; set; }
    }
}
