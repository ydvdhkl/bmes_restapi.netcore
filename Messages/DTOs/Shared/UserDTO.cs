﻿namespace BMES_RestAPI.Messages.DTOs.Shared
{
    public class UserDTO
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
    }
}
