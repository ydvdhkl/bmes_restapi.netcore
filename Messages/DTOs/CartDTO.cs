﻿using System;
using System.Collections.Generic;

namespace BMES_RestAPI.Messages.DTOs
{
    public class CartDTO
    {
        public CartDTO()
        {
            CartItems = new List<CartItemDTO>();
        }
        public long Id { get; set; }
        public string UniqueCartId { get; set; }
        public int CartStatus { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public IEnumerable<CartItemDTO> CartItems { get; set; }
    }
}
