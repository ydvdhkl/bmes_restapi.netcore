﻿namespace BMES_RestAPI.Messages.DTOs
{
    public class CartItemDTO
    {
        public long Id { get; set; }
        public long CartId { get; set; }
        public ProductDTO Product { get; set; }
        public int Quantity { get; set; }
    }
}
