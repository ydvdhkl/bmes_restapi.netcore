﻿using BMES_RestAPI.Messages.DTOs;
using System.Collections.Generic;

namespace BMES_RestAPI.Messages.Response.Product
{
    public class FetchProductsResponse : ResponseBase
    {
        public int ProductsPerPage { get; set; }
        public bool HasPreviousPages { get; set; }
        public bool HasNextPages { get; set; }
        public int CurrentPage { get; set; }
        public int[] Pages { get; set; }
        public IEnumerable<ProductDTO> Products { get; set; }
    }
}
