﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Product
{
    public class CreateProductResponse : ResponseBase
    {
        public ProductDTO Product { get; set; }
    }
}
