﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Product
{
    public class DeleteProductResponse : ResponseBase
    {
        public ProductDTO Product { get; set; }
    }
}
