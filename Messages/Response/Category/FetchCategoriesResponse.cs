﻿using BMES_RestAPI.Messages.DTOs;
using System.Collections.Generic;

namespace BMES_RestAPI.Messages.Response.Category
{
    public class FetchCategoriesResponse : ResponseBase
    {
        public int CategoriesPerPage { get; set; }
        public bool HasPreviousPages { get; set; }
        public bool HasNextPages { get; set; }
        public int CurrentPage { get; set; }
        public int[] Pages { get; set; }
        public IEnumerable<CategoryDTO> Categories { get; set; }
    }
}
