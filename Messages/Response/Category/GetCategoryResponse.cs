﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Category
{
    public class GetCategoryResponse : ResponseBase
    {
        public CategoryDTO Category { get; set; }
    }
}
