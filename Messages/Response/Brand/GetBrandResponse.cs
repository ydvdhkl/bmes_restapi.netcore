﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Brand
{
    public class GetBrandResponse : ResponseBase
    {
        public BrandDTO Brand { get; set; }
    }
}
