﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Brand
{
    public class DeleteBrandResponse : ResponseBase
    {
        public BrandDTO Brand { get; set; }
    }
}
