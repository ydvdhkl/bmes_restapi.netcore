﻿using BMES_RestAPI.Messages.DTOs;
using System.Collections.Generic;

namespace BMES_RestAPI.Messages.Response.Order
{
    public class FetchOrdersResponse : ResponseBase
    {
        public int OrdersPerPage { get; set; }
        public bool HasPreviousPages { get; set; }
        public bool HasNextPages { get; set; }
        public int CurrentPage { get; set; }
        public int[] Pages { get; set; }
        public IEnumerable<OrderDTO> Orders { get; set; }
    }
}
