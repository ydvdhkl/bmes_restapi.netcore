﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Order
{
    public class GetOrderResponse : ResponseBase
    {
        public OrderDTO Order { get; set; }
    }
}
