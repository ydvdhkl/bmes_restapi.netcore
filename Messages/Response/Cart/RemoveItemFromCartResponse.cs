﻿namespace BMES_RestAPI.Messages.Response.Cart
{
    public class RemoveItemFromCartResponse : ResponseBase
    {
        public long CartItemId { get; set; }
    }
}
