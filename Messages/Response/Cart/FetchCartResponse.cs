﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Cart
{
    public class FetchCartResponse : ResponseBase
    {
        public CartDTO Cart { get; set; }
    }
}
