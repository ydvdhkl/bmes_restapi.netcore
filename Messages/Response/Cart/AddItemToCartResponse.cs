﻿using BMES_RestAPI.Messages.DTOs;

namespace BMES_RestAPI.Messages.Response.Cart
{
    public class AddItemToCartResponse : ResponseBase
    {
        public CartItemDTO CartItem { get; set; }
    }
}
