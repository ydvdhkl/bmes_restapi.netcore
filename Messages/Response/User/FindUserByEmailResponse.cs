﻿namespace BMES_RestAPI.Messages.Response.User
{
    public class FindUserByEmailResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
