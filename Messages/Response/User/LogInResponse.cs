﻿namespace BMES_RestAPI.Messages.Response.User
{
    public class LogInResponse : ResponseBase
    {
        public string Token { get; set; }
    }
}
