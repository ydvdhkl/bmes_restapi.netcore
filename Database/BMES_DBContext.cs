﻿using BMES_RestAPI.Models.Address;
using BMES_RestAPI.Models.Cart;
using BMES_RestAPI.Models.Customer;
using BMES_RestAPI.Models.Order;
using BMES_RestAPI.Models.Product;
using BMES_RestAPI.Models.Shared;
using Microsoft.EntityFrameworkCore;

namespace BMES_RestAPI.Database
{
    //This class is Mapping the Object to the database table
    public class BMES_DBContext :DbContext
    {
        public BMES_DBContext(DbContextOptions<BMES_DBContext> options) : base(options) { }

        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CarItems { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
    }
}
