﻿using BMES_RestAPI.Models.Shared;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BMES_RestAPI.Database
{
    public class BMES_Identity_DBContext :IdentityDbContext<User>
    {
        public BMES_Identity_DBContext(DbContextOptions<BMES_Identity_DBContext> options) : base(options) { }
    }
}
